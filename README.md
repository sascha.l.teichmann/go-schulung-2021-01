# Go Schulung für das OpenSlides Team - 19.01.2021

* [Agenda](agenda.md)
* [Links](links.md)
* [Code-Beispiele](https://gitlab.com/sascha.l.teichmann/go-examples)
* [Live-Coding-Beispiele](workspace/src/README.md)


